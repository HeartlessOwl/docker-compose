<h1> Docker Compose with "pipeline" </h1>
 
This project provides a Dockerized environment for an application using Nginx and PHP. It employs Docker Compose for managing dependencies and simplifies running the application in both development and production settings. Containers are set up for the local development.

The docker-compose.prod.yml file is used to configure possible additional settings for the production environment.

<h2> To run locally: </h2>

Prod version: docker compose -f docker-compose.yml -f docker-compose.prod.yml up

Dev version:  docker compose up

The CI/CD pipeline utilizes GitLab CI/CD and incorporates different build stages for the main and dev branches, corresponding to the local commands mentioned above.

<h2> References: </h2>

CI/CD - https://docs.gitlab.com/ee/ci/yaml/#rules 

Gitlab CI with docker - https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-socket-binding 

Docker Compose - https://marc.it/dockerize-application-with-nginx-and-php8/ 

<h2> Suggestions to improve: </h2>
A future improvement would be to utilize Docker Compose profiles and environment variables within a single docker-compose.yml file for managing both development and production environments. This approach simplifies configuration and streamlines deployment processes. 

The CI/CD pipeline conditions will be switched to trigger only on merge requests to streamline the process and exclude unnecessairy resource consumption upon pushing the small changes, that do not require to be tested out/deployed immediately. 


<h2> Overall Process </h2>

1. Developer makes changes locally and pushes the changes to feature branch.

2. Upon merge to Dev, the pipeline triggers and runs separate build and test stages. 

3. If succesfull, it may be decided to merge DEV to PROD, which will trigger the PROD condition to build, test and deploy containers. (Can add a step to push the images to the registry in case they are developed locally).


<b> Working Version on Docker Compose v2.25.0 </b>

