<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Shenanigans with PHP!</title>
  <style>
    body {
      font-family: sans-serif;
      margin: 0;
      padding: 20px;
    }
    h1 {
      text-align: center;
    }
    .change-color {
      color: blue;
      cursor: pointer;
    }
  </style>
</head>
<body>
  <h1>Get ready for some PHP shenanigans!</h1>
  <p>Click the text below to change its color (powered by PHP!)</p>
  <p class="change-color">This text can be clicked!</p>

  <?php
  // Define some colors for shenanigans
  $colors = array('red', 'green', 'yellow', 'purple', 'orange');

  // Check if a "changeColor" variable is set in the URL
  if (isset($_GET['changeColor'])) {
    $newColorIndex = array_rand($colors); // Get a random index from the colors array
    $newColor = $colors[$newColorIndex]; // Access the random color using the index
  } else {
    $newColor = 'blue'; // Default color if not clicked
  }
  ?>

  <script>
    const changeColorElement = document.querySelector('.change-color');

    changeColorElement.addEventListener('click', () => {
      // Update the URL with a "changeColor" parameter to trigger the PHP logic again
      window.location.href = "?changeColor=true";
    });
  </script>
</body>
</html>
